import io.cucumber.java.en.Given;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static io.restassured.RestAssured.given;

public class MyStepdefs {

    Response response;

    @Given("User does a get call to fetch all the bookings")
    public void userDoesAGetCallToFetchAllTheBookings() {

        response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON) // optional
                .when()
                .get("https://restful-booker.herokuapp.com/booking");

        System.out.println(response.prettyPrint());

    }

    @Given("User does a post call to create a booking")
    public void userDoesAPostCallToCreateABooking() throws IOException {

        String jsonPath = System.getProperty("user.dir") + "/src/test/java/payload/createBookingPayload.json";
        String body = new String(Files.readAllBytes(Paths.get(jsonPath)));
        body = body.replace("<Name>", "Anupama");

        response = given()
                .contentType(ContentType.JSON)
//                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("https://restful-booker.herokuapp.com/booking");

        System.out.println(response.prettyPrint());// 904 booking id
    }
}
